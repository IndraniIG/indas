package com.techprimers.springbootneo4jexample1.resource;

import com.techprimers.springbootneo4jexample1.model.User;
import com.techprimers.springbootneo4jexample1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
//@RequestMapping("/rest/neo4j/user")
public class UserResource {

    @Autowired
    UserService userService;


    @RequestMapping("/rest/neo4j/reviewer")
    public Collection<User> getAllReviewers() {
        return userService.getAllR();
    }

    @RequestMapping("/rest/neo4j/actor")
    public Collection<User> getAllActors(){
    	return userService.getAllA();
    }
    
    @RequestMapping("/rest/neo4j/director")
    public Collection<User> getAllDirectors(){
    	return userService.getAllD();
    }

}
