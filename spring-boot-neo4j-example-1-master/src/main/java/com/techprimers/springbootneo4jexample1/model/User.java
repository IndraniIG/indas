package com.techprimers.springbootneo4jexample1.model;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;


import java.util.List;

@NodeEntity
public class User {

    @GraphId
    private Long id;
    private String name;
    private Integer age;
    private String role;

    @Relationship(type = "RATED_BY", direction = Relationship.INCOMING)

    private List<Movie> movies;

    @Relationship(type = "DIRECTED", direction = Relationship.OUTGOING)
    private List<Movie> movies_d;
    
    @Relationship(type = "ACTED_IN", direction = Relationship.OUTGOING)
    private List<Movie> movies_a;
    
    public List<Movie> getMovies() {
        return movies;
    }
    
    public List<Movie> getDirectedMovies()
    {
    	return movies_d;
    }
    
    public List<Movie> getActedMovies(){
    	return movies_a;
    }
    public User() {

    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }
    
    public String getRole() {
    	return role;
    }
}
