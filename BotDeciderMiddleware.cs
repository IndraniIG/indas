﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;

namespace PromptUsersForInput
{
    public class BotDeciderMiddleware : IMiddleware
    {
        private readonly BotState _userState;
        private readonly BotState _conversationState;
        Task saveContext;
        int flag = 0;
        
        public BotDeciderMiddleware(ConversationState conversationState, UserState userState)
        {
                _conversationState = conversationState;
                _userState = userState;

        }


        public async Task OnTurnAsync(ITurnContext turnContext, NextDelegate nextDelegate, CancellationToken cancellationToken)
        {
            if (turnContext.Activity != null)
            {
                //if (turnContext.Activity.From == null)
                //{
                //    turnContext.Activity.From = new ChannelAccount();
                //}

                //if (string.IsNullOrEmpty((string)turnContext.Activity.From.Properties["role"]))
                //{
                //    turnContext.Activity.From.Properties["role"] = "user";
                //}

                if (turnContext.Activity.Text == "stop" && turnContext.Activity.Text != "resume")

                {
                    flag = 1;
                }
                else if (turnContext.Activity.Text == "resume")
                    flag = 0;

                if (flag == 1)
                {
                    await turnContext.SendActivityAsync("The bot has stopped");
                    await turnContext.SendActivityAsync("Press resume to continue");
                    await nextDelegate(cancellationToken).ConfigureAwait(true);
                    await _conversationState.SaveChangesAsync(turnContext);
                }
                else
                    await nextDelegate(cancellationToken).ConfigureAwait(false);

                //if (turnContext.Activity.Text != "resume")
                //    {
                //       // flag = 1;
                //        await turnContext.SendActivityAsync("The bot has stopped");
                //        await turnContext.SendActivityAsync("Press resume to continue");
                //        saveContext = _conversationState.SaveChangesAsync(turnContext);
                //        //var saveContext = _conversationState.LoadAsync(turnContext,true,cancellationToken);
                //        await nextDelegate(cancellationToken).ConfigureAwait(true);
                //        //await base.OnTurnAsync(turnContext, nextDelegate,cancellationToken);

                //    }
                //    else if(turnContext.Activity.Text == "resume")
                //    {
                //        //var sContext = saveContext;
                //        flag = 0;

                //    }
                //}
                ////else if (turnContext.Activity.Text == "resume")
                ////{
                ////    
                ////}




                //LogActivity(transcript, CloneActivity(turnContext.Activity));
            }
        
                
            }
        }
    }
